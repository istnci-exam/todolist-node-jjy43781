// 파이어베이스 초기화하는 별도의 모듈
const admin = require('firebase-admin');

const serviceAccount = require("./myproject-65070-firebase-adminsdk-94pub-67c35ff40d.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

module.exports = db;