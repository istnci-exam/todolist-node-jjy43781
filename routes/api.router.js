const router = require('express').Router();
const TodolistService = require("../services/Todolist.service");

router.get("/", async (req, res) => {
    try {
        const user_id = req.headers.user_id;
        const result = await TodolistService.getTodolist(user_id);

        res.status(200).send(result);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.post("/", async (req, res) => {
    try {
        const todolist = req.body;
        const result = await TodolistService.updateTodolist(todolist);

        res.status(200).send(result);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.post("/conversation", async (req, res) => {
    try {

        debugger;
    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.post("/message", async (req, res) => {
    try {
        // 유저id를 받아
        const user_id = req.body.user_id;
        // 유저id로 대화방을 만들고
        const resultConversation = await TodolistService.openConversation(user_id);

        //대화방의 정보로
        const conversation_id = resultConversation.data.conversation.id;
        //메세지를 설정해서 보낸다
        const resultMessage = await TodolistService.sendMessage(conversation_id);

        res.status(200).send(resultMessage.data);

        // const conversation_id = req.body.conversation_id;
    }  catch (error) {
        res.status(500).send(error.message);
    }
});

module.exports = router;