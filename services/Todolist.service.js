const db = require("../config/firebase");
const axios = require("axios");

exports.getTodolist = async(user_id) => {
    try {
        const docRef = db.collection("Todolist").doc(user_id);
        const doc = await docRef.get();

        if(doc.exists) {
            const todolist = doc.data();
            return todolist;
        } else {
            docRef.set({
                user_id: user_id,
                user_name: null,
                items: [],
            });
        }
        return;
    } catch (error) {
       return error;
    }

};

 // 1. 자체로 익스포트시키는 방법
 exports.updateTodolist = async(todolist) => {
    // const updateTodolist = async() => {
    try {
        const docRef = db.collection("Todolist").doc(todolist.user_id);
        await docRef.set(todolist);

        const doc = await docRef.get();

        if (doc.exists) {
            const updateTodolist = doc.data();
            return updateTodolist;
        }
        return;
    } catch (error) {
      return error;
    }
};

exports.openConversation = async(user_id) => {
    // const updateTodolist = async() => {
    try {
        const options = {
            url: "https://api.kakaowork.com/v1/conversations.open",
            method:"POST",
            headers: {
                "Authorization": "Bearer 656d6498.d54b9300a3b0453ea63001db0ba3a1ff",
                "Content-Type": "application/json"
              },

            data: {
                user_id : "2574151",
            },
        }

        return axios(options);
    } catch (error) {
      return error;
    }
};

exports.sendMessage = async(conversation_id) => {
    try {
        const options = {
            url: "https://api.kakaowork.com/v1/messages.send",
            method:"POST",
            headers: {
                "Authorization": "Bearer 656d6498.d54b9300a3b0453ea63001db0ba3a1ff",
                "Content-Type": "application/json"
              },
              data:{
                conversation_id: conversation_id,
                text: "Push alarm message",
                blocks: [
                    {
                        type: "header",
                        text: "Todo 생성 알람",
                        style: "blue",
                    },
                    {
                        type: "text",
                        text: "새로운 할일이 등록됐습니다.",
                        markdown: true,
                    },
                    {
                        type: "button",
                        text: "버튼",
                        style: "default",
                        action_type: "open_mini_inapp_browser", //카카오에서 우리회사만열어준 위아래 바 없앤거
                        value: "https://react-todolist-jjy4378.cfapps.us10.hana.ondemand.com/", //리액트로열리게합시당
                    },
                    {
                        type: "description",
                        term: "일시",
                        content: {
                            type: "text",
                            text: "",
                            markdown: false,
                        },
                        accent: true,
                    },
                ]
            }     
              };
            return axios(options); 
    } catch (error) {
        return error;
      }
};

// // 2.모듈익스포트 사용방법
// module.exports = {
//     getTodolist: getTodo,
//     updateTodolist: updateTodo,
// };