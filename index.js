const express = require('express'); //express 정의
const app = express(); //express 사용
const cors = require('cors'); //cors 정의
const bodyParser = require('body-parser'); //bodyParser 정의

const port = process.env.PORT || 5000; //포트연결

app.use(cors()); //CORS 제한을 해결하기 위한 미들웨어 등록
app.use(bodyParser.json()); //API요청에서 받은 body값을 파싱을 위한 미들웨어 등록

// 라우터 객체 관리
const apiRouter = require("./routes/api.router");
app.use("/api/v1/todolist", apiRouter);

// npm start 서버실행 & 포트연결메세지
app.listen(port, () => {
    console.log(`Server Listening Port ${port}`);
});

